package com.jfbeauvais.duplicate.file.finder

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File

internal class DuplicateFileFinderTest {

    private val resourcesRoot = "src/test/resources"
    private val root1 = File(resourcesRoot, "root1")
    private val root2 = File(resourcesRoot, "root2")
    private val roots = sequenceOf(root1, root2)

    @Test
    fun listDuplicates() {
        val duplicates = roots.listDuplicates().asIterable()
        val assertion = assertThat(duplicates)
            .hasSize(2)
            .extracting<List<String>> { list -> list.map { it.path } }

        assertion.anySatisfy {
            assertThat(it).containsExactlyInAnyOrder("$root1/1-char-a.txt", "$root2/1-char-also-a.txt")
        }
        assertion.anySatisfy {
            assertThat(it).containsExactlyInAnyOrder("$root1/sub-dir/1-char-b.txt", "$root2/1-char-b.txt")
        }
    }

    @Test
    fun walkFiles() {
        val names = roots.walkFiles().map { it.name }.toList()
        assertThat(names)
            .hasSize(6)
            .contains("1-char-c.txt", "1-char-b.txt", "1-char-a.txt", "2-char-c.txt", "1-char-also-a.txt", "1-char-b.txt")
            .doesNotContain("empty.txt", root1.name) // Do not contain empty files nor folders
    }

    @Test
    fun duplicatesBy() {
        val oneChar = File(root1, "1-char-a.txt")
        val oneChar2 = File(root2, "1-char-b.txt")
        val twoChar = File(root1, "2-char-c.txt")
        val duplicatesByLength = sequenceOf(oneChar, oneChar2, twoChar).duplicateBy { it.length() }.asIterable()
        assertThat(duplicatesByLength)
            .hasSize(1)
            .first().asList().extracting("name")
            .containsExactlyInAnyOrder(
                "1-char-a.txt",
                "1-char-b.txt")
    }

    @Test
    fun md5Hash() {
        val md5A = File(root1, "1-char-a.txt").md5Hash()
        val md5B = File(root2, "1-char-b.txt").md5Hash()
        val md5AlsoA = File(root2, "1-char-also-a.txt").md5Hash()
        assertThat(md5A).isEqualTo(md5AlsoA).isNotEqualTo(md5B)
    }
}