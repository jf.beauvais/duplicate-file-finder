package com.jfbeauvais.duplicate.file.finder;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DuplicateFileFinderJavaTest {

    private String resourcesRoot = "src/test/resources";
    private File root1 = new File(resourcesRoot, "root1");
    private File root2 = new File(resourcesRoot, "root2");

    @Test
    public void verifyJavaCompatibility(){
        Iterable<List<File>> duplicates = DuplicateFileFinder.listDuplicates(Arrays.asList(root1, root2));
        assertThat(duplicates).hasSize(2);
    }
}
