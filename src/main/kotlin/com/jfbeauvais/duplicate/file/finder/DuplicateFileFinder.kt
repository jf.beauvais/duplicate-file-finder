@file:JvmName("DuplicateFileFinder")
package com.jfbeauvais.duplicate.file.finder

import java.io.File
import java.security.MessageDigest

fun Iterable<File>.listDuplicates() = asSequence().listDuplicates().asIterable()

fun Sequence<File>.listDuplicates() = walkFiles()
    // Select duplicate by file size
    .duplicateBy { it.length() }
    // Select duplicate by file MD5
    .flatMap{ files -> files.asSequence().duplicateBy { it.md5Hash() } }

// Concat multiple file streams
fun Sequence<File>.walkFiles() = map { it.walkTopDown() }.reduce { a: Sequence<File>, b -> a + b}
    .filter { it.isFile }
    .filter { it.length() > 0 }

fun <S> Sequence<File>.duplicateBy(selector: (File) -> S) = groupBy(selector)
    .filterValues { it.size > 1 } // remove unique entries from stream
    .values.asSequence()

fun File.md5Hash() : Int {
    val md = MessageDigest.getInstance("MD5")
    inputStream().buffered().use { md.update(it.readBytes()) }
    return md.digest()?.contentHashCode() ?: 0
}


